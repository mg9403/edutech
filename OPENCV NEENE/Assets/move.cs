﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    int Speed = 50; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float fMove = Time.deltaTime * Speed;
        transform.Translate(Vector3.right * fMove);
    }
}
